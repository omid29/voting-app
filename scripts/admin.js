let Distinct_Vote_Question_Array =[]

function viewEditForm() {
    document.getElementById("EditTopicForm").style.display = "block";

    let selectVoteTopic = document.getElementById("adminVoteTopics");
    let selectVoteTopicResult = selectVoteTopic.options[selectVoteTopic.selectedIndex].text;
    Distinct_Vote_Question_Array.map((item)=>{
        const trimmedCheck = selectVoteTopicResult.substring(0,51)
        if(item.question.includes(trimmedCheck)){
            selectVoteTopicResult = item.question
        }
    })
    document.getElementById("updateInputVoteTopic").value = selectVoteTopicResult;
}

function deleteTopic() {
    try {
        let selectVoteTopic = document.getElementById("adminVoteTopics");
        let selectVoteTopicResult = selectVoteTopic.options[selectVoteTopic.selectedIndex].text;
        Distinct_Vote_Question_Array.map((item)=>{
            const trimmedCheck = selectVoteTopicResult.substring(0,51)
            if(item.question.includes(trimmedCheck)){
                selectVoteTopicResult = item.question
            }
        })
        let del = db.collection("TestTopic").doc(selectVoteTopicResult).delete();
        console.log(del);
    }
    catch (error) {
        console.log(error)
    }
}

async function updateTopic() {
    try {
        let selectVoteTopic = document.getElementById("adminVoteTopics");
        let selectVoteTopicResult = selectVoteTopic.options[selectVoteTopic.selectedIndex].text;
        let voteTitle = ""
        let topicExpiryDate = ""
        // console.log(selectVoteTopicResult,"check the reulst")
        Distinct_Vote_Question_Array.map((item)=>{
            const trimmedCheck = selectVoteTopicResult.substring(0,51)
            if(item.question.includes(trimmedCheck)){
                selectVoteTopicResult = item.question
            }
        })
        //update action
        voteTitle = EditTopicForm["updateInputVoteTopic"].value;

        topicExpiryDate = EditTopicForm["updateDateVoteTopic"].value;
        topicExpiryDate = new Date(topicExpiryDate).getTime()

        // console.log(voteTitle, topicExpiryDate)
        if (typeof topicExpiryDate != "number" || voteTitle.length == 0) {
            // pass the ref of button here
            
            buttonRef.disabled = true;
            alert("Please Fill the inputs")
            setTimeout(() => {
                buttonRef.disabled = false;
            }, 500);
        }
        else {
            EditTopicForm.reset()
            const topicRef = db.collection('TestTopic').doc(selectVoteTopicResult);
            const res = await topicRef.set({
                question: voteTitle,
                expiryDate: topicExpiryDate,
                totalYes: 0,
                totalNo: 0,
            }, { merge: true });
            alert(`Topic ${voteTitle} Has Been Added`)
        }
    }
    catch (error) {
        console.log(error)
    }
}

function addTopic() {


    const adminAddTopic = document.querySelector("#adminAddTopic");
    const addTopicForm = document.querySelector("#addTopicForm");
    let voteTitle = ""
    let topicExpiryDate = ""
    addTopicForm.addEventListener("submit", async (e) => {
        e.preventDefault();
    })

    //update action
    voteTitle = addTopicForm["inputVoteTopic"].value;
    topicExpiryDate = addTopicForm["inputDateVoteTopic"].value;
    topicExpiryDate = new Date(topicExpiryDate).getTime()
    // console.log(voteTitle, topicExpiryDate)
    alert(`Topic ${voteTitle} Has Been Added`)
    addTopicForm.reset()
    //     const topicRef = db.collection('VoteTopics').doc(voteTitle);
    //     const res = await topicRef.set({
    //         question: voteTitle,
    //         expiryDate: topicExpiryDate,
    //         totalNumberOfVote: 0,
    //         totalYes: 0,
    //         totalNo: 0,

    //         capital: true
    //     }, { merge: true });


}

function editTopic() {
    const addTopicForm = document.querySelector("#addTopicForm");
    addTopicForm.addEventListener("submit", async (e) => {
        e.preventDefault();
        window.location.href = "./AdminVoteTitleEdit.html"

    })
}

function bulkSignUp(email, defaultPassword) {
    auth.createUserWithEmailAndPassword(email, defaultPassword).then(cred => {
        // window.alert("User Successfully Registred")
        // console.log(`User ${email} Has been Registered`);

    })
        .catch(({ message, code }) => {
            // alert(message)
            // console.log(message);
        })
}
async function bulkAddUser(email, shares) {
    const userTableRef = db.collection("Users")
    try {
        await userTableRef.doc(email).set({
            VoteTitles: [],
            Email: email,
            Shares: shares
        });
        // console.log("User Added")
    }
    catch (e) {
        // console.log(e)
    }

}

async function exportUsersToCsv() {
    const exportBtn = document.querySelector("exportUsersToCsv")
    // console.log("IMHERE")
    const usersRequest = db.collection("Users")
    const votedoc = await usersRequest.get();
    let usersArray = []
    votedoc.forEach(doc => {
        const share = doc.data().Shares
        const email = doc.data().Email
        usersArray.push({ share, email })
    });
    // console.log(usersArray)
    // JSON.stringify(usersArray)
    strArray = []
    usersArray.map((item) => {
        // console.log(item)
        strArray.push([item.share, item.email])

    })
    // console.log(strArray)
    // let csvContent = "data:text/csv;charset=utf-8" + item && item.share.join(",") + item.email.join(",").join("\n")
    // var encodedUri = encodeURI(csvContent);    
    let csvContent = `data:text/csv;charset=utf-8,${strArray && strArray.map(e => e.join(",")).join("\n")}`;
    var encodedUri = encodeURI(csvContent);
    window.open(encodedUri);
}
async function exportFullUsersToCsv() {
    const exportBtn = document.querySelector("exportUsersToCsv")
    // console.log("IMHERE")
    const usersRequest = db.collection("Users")
    const votedoc = await usersRequest.get();
    let usersArray = []
    votedoc.forEach(doc => {
        const share = doc.data().Shares
        const email = doc.data().Email
        const voteTitle = doc.data() && doc.data().VoteTitles
        // console.log(voteTitle,"checking the vote titles")
        let vote = ""
        let voteTopic = ""
        voteTitle && voteTitle.map((item) => {
            vote = item.Vote
            voteTopic = item.VoteTopic
        })
        usersArray.push({ email, voteTopic, vote, share })
    });
    // console.log(usersArray)
    // JSON.stringify(usersArray)
    strArray = []
    usersArray.map((item) => {
        // console.log(item)
        strArray.push([item.email, item.voteTopic, item.vote, item.share])

    })
    // console.log(strArray)
    // let csvContent = "data:text/csv;charset=utf-8" + item && item.share.join(",") + item.email.join(",").join("\n")
    // var encodedUri = encodeURI(csvContent);    
    let csvContent = `data:text/csv;charset=utf-8,${strArray && strArray.map(e => e.join(",")).join("\n")}`;
    var encodedUri = encodeURI(csvContent);
    window.open(encodedUri);
}

async function populateAdminVoteTopic() {
    let voteTopic = document.querySelector("#adminVoteTopics");
    let Html = `<option value="">-- select an option --</option>`
    const topicRef = db.collection('VoteTopics');
    const snapshot = await topicRef.get();
    snapshot.forEach(snapshot => {
        let eachTopic = snapshot.data() && snapshot.data().question
        const expiryDate = snapshot.data() && snapshot.data().expiryDate
        let voteQuestionArray = []
        voteQuestionArray.push({question:eachTopic})
        Distinct_Vote_Question_Array = [...new Set(voteQuestionArray)]
        const dateNow = new Date().getTime()
        if (dateNow > expiryDate) {
            // alert(`Vote Topic ${eachTopic} Has Been Expired`)
        }
        else {
            if (eachTopic && eachTopic.length > 81) {
                let trimmedVoteTopic = eachTopic.substring(0, 81)
                trimmedVoteTopic = `${trimmedVoteTopic} ...`
                const option = `
            <option value=${eachTopic}>${trimmedVoteTopic}</option>
            `
                Html += option
            }
            else {
                const option = `
            <option value=${eachTopic}>${eachTopic}</option>
            `
                Html += option
            }
            // const option = `
            // <option value=${eachTopic}>${eachTopic}</option>
            // `
            // // selectElement.options[selectElement.selectedIndex].text;
            // Html += option

        }



        // console.log(snapshot.id, '=>', snapshot.data());
        // const eachTopic = snapshot.data() && snapshot.data().question
        // const expiryDate = snapshot.data() && snapshot.data().expiryDate
        // const dateNow = new Date().getTime()
        // console.log(expiryDate, dateNow)
        // const option = `
        //     <option value=${eachTopic}>${eachTopic}</option>
        //     `
        // selectElement.options[selectElement.selectedIndex].text;
        // Html += option
        // if (dateNow > expiryDate) {
        //     // alert(`Vote Topic ${eachTopic} Has Been Expired`)
        // }
        // else {

        voteTopic.innerHTML = Html
        // }
    });

    // voteTopic.innerHTML = Html
    // var user = auth.currentUser;
    // if(user){
    //     username.innerHTML = `Welcome ${userEmail}`;
    //     let username = document.querySelector(".usrname")
    // }
    // else console.log("USER NOT FOUND")


}

const setupAdminTable = (data) => {
    let resultTable = document.querySelector("#allVotes");
    let html = ""
    let email = ""
    singleItem = data
    email = singleItem && singleItem.Email
    shares = singleItem && singleItem.Shares
    const totalShares = 718804
    // const shareWeight = shares / totalShares
    if (singleItem && singleItem.VoteTitles) {
        singleItem && singleItem.VoteTitles.map((item) => {
            // console.log(item, "EACH ITEM ")
            const tr = `
                         <tr>
                             <td>${email}</td>
                             <td>${item.VoteTopic}</td>
                             <td>${item.Vote}</td>
                             <td>${shares}</td>
                         </tr>
                    `;
            html += tr
        })
        resultTable.insertAdjacentHTML('beforeend', html);
        $(document).ready(function () {
            $('#AdminTable').DataTable();
            $('.dataTables_length').addClass('bs-select');
        });

    }
}


async function getLiveResult() {

    var selectElement = document.getElementById("adminVoteTopics");
    var voteTopicResult = document.getElementById("voteTopicResult");
    var totalYesResult = document.getElementById("totalYesResult");
    var totalNoResult = document.getElementById("totalNoResult");
    var AdminResultTable = document.getElementById("AdminResultTable");
    var votedSharesTotal = document.getElementById("votedSharesTotal");
    var votedSharesTotalInPercentage = document.getElementById("votedSharesTotalInPercentage");
    var totalVotersCountElement = document.getElementById("totalVoters");
    var totalVotersInPercentageElement = document.getElementById("totalVotersInPercentage");



    AdminResultTable.style.display = "inline-table"
    let liveResults = ""

    let selectElementResult = selectElement.options[selectElement.selectedIndex].text;
    // console.log(selectElementResult, "RESULT OF DROP DOWN SELECT")
    //CHECKING THE DROP DOWN TEXT
    if (selectElementResult.includes("-- select an option --")) {
        document.getElementById("checkLiveResults").disabled = true
        setTimeout(() => {
            document.getElementById("checkLiveResults").disabled = false
        }, 500);
    }
    else {
        const usersRef = db.collection('Users');
        const usersRequest = await usersRef.get();
        let totalVoters = []
        let totalYesVoteCounter = 0;
        let totalNoVoteCounter = 0;
        let totalUsers = 0
        let totalShares = 0
        let totalVotersCount = 0
        usersRequest.forEach(user => {
            const email = user.data().Email;
            const share = user.data().Shares;
            totalUsers += 1;
            totalShares += parseInt(shares)
            const voteTitles = user.data() && user.data().VoteTitles;
            Distinct_Vote_Question_Array.map((item)=>{
                const trimmedCheck = selectElementResult.substring(0,51)
                if(item.question.includes(trimmedCheck)){
                    selectElementResult = item.question
                }
            })
            if (voteTitles && voteTitles.length > 0) {
                voteTitles.map(async (eachVoteItem) => {
                    let existedVote = voteTitles.filter(eachitem => eachitem.VoteTopic === selectElementResult)
                    if (existedVote && existedVote.length > 0 && eachVoteItem && eachVoteItem.VoteTopic == selectElementResult) {
                        const topicRef = db.collection('VoteTopics').doc(selectElementResult)
                        const snapshot = await topicRef.get();
                        const specificTopicData = snapshot.data()
                        let totalYesVote = snapshot.data() & snapshot.data().totalYes
                        totalVoters.push(specificTopicData)
                        if (specificTopicData.question == eachVoteItem.VoteTopic) {
                            if (existedVote[0].Vote == "YES") {
                                totalYesVote += parseInt(share)
                                totalVotersCount += 1
                                totalYesVoteCounter += parseInt(share)
                            }
                            else if (existedVote[0].Vote == "NO") {
                                totalVotersCount += 1
                                totalNoVoteCounter += parseInt(share)
                            }
                        }

                    }
                    // console.log(totalYesVoteCounter)
                    // console.log(totalNoVoteCounter)
                    voteTopicResult.innerHTML = selectElementResult.substring(0, 19);
                    totalYesResult.innerHTML = totalYesVoteCounter;
                    totalNoResult.innerHTML = totalNoVoteCounter;
                    votedSharesTotal.innerHTML = totalYesVoteCounter + totalNoVoteCounter;
                    const totalShareInPercentage = ((totalYesVoteCounter + totalNoVoteCounter) / 718804) * 100;
                    const totalVotersInPercentage = (totalVotersCount / totalUsers) * 100;
                    votedSharesTotalInPercentage.innerHTML = totalShareInPercentage.toFixed(2) + "%";
                    totalVotersCountElement.innerHTML = totalVotersCount;
                    totalVotersInPercentageElement.innerHTML = totalVotersInPercentage.toFixed(2) + "%";
                    // totalVotersPercentage.innerHTML = totalVoters;
                })

            }

        })

    }





}



// window.onload = function () {


// }   
