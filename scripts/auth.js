
function signup() {
    // const signupForm = document.querySelector("#signupForm");
    // signupForm.addEventListener("submit", (e) => {
    //     e.preventDefault();
    //     //get user infor 
    //     const email = signupForm["inputEmailSignup"].value;
    //     const Password = signupForm["inputPasswordSignup"].value;
    //     auth.createUserWithEmailAndPassword(email, Password).then(cred => {
    //         console.log(cred.user)
    //         signupForm.reset()
    //         // window.alert("User Successfully Registred")
    //         window.location.href = "../index.html"
    //         // verificationEmail()
    //     })
    //         .catch(({ message, code }) => {
    //             console.log(message)
    //             let errMessage = document.querySelector(".signup-error");
    //             errMessage.innerHTML = message
    //         })
    // })
}

function verificationEmail() {
    var user = auth.currentUser;
    user.sendEmailVerification().then(function () {
        // window.alert("A Verification Email Has Been Sent")
    }).catch(function ({ message }) {
        // console.log(message)
    });
}

function signin() {
    const signinForm = document.querySelector("#signinForm");
    signinForm.addEventListener("submit", (e) => {
        // console.log("herehhhhhhh")
        e.preventDefault();
        const email = signinForm["inputEmail"].value;
        const Password = signinForm["inputPassword"].value;
        let username = ""
        auth.signInWithEmailAndPassword(email, Password).then((cred) => {
            // console.log(cred)
            signinForm.reset();
            username = cred && cred.user && cred.user.email
            // console.log(username)
            if (username != "cwerner@truststamp.ai") {

                window.location.href = "./pages/UserDashboardMenu.html";
            }
            else {
                window.location.href = "./pages/AdminDashboard.html";
            }

        })
            .catch(({ message, code }) => {
                // console.log(message)
                let errMessage = document.querySelector(".signin-error");
                errMessage.innerHTML = message
            })
    })
}

function logout() {

    //e.preventDefault();

    auth.signOut().then(() => {
        // console.log("im here in logout")
        // window.alert("USER HAS SIGNED OUT");
        window.location.href = "../index.html"
    })
        .catch(({ message }) => {
            // console.log(message)
        })
    // logout.addEventListener("click", (e) => {


    // })
}
function forgotPassword() {
    // var emailAddress = "osarei@truststamp.net";
    // var actionCodeSettings = {
    //     // After password reset, the user will be give the ability to go back
    //     // to this page.
    //     url: 'file:///Users/omid/Desktop/Truststamp/Vote-Application/index.html',
    //     handleCodeInApp: false
    // };
    const forgotPasswordForm = document.querySelector("#forgotPasswordForm");
    // forgotPasswordForm.addEventListener("submit", (e) => {
    $("#forgotPasswordForm").one('submit', function (e) {
        document.getElementById("resetPassword").disabled = true;
        e.preventDefault()
        // $("#resetPassword").onclick = "";
        const email = forgotPasswordForm["inputEmailForgetPassword"].value;
        // console.log(email)
        auth.sendPasswordResetEmail(email).then(function () {
            // forgotPasswordForm.reset()
            window.alert("An Email Has Been Sent Please Check your Email")

            window.location.href = "../index.html"
        }).catch(function ({ message }) {
            // console.log(message)
            let errMessage = document.querySelector(".forgotPassword-error");
            errMessage.innerHTML = message
        });
    })
    // })
}
async function getCurrentUserVoteHistory(userEmail) {

    const request = db.collection("Users").doc(userEmail)
    const topicRequest = db.collection("VoteTopics")
    const doc = await request.get();
    const votedoc = await topicRequest.get();
    let userData = {}
    let voteTopicArray = []
    votedoc.forEach(doc => {
        // console.log(doc.id, '=>', doc.data());
        voteTopicArray.push(doc.data())
    });
    if (!doc.exists) {
        // console.log('No such document!');
    } else {
        // console.log('Document data:', votedoc.data());
        // console.log(doc.data())
        userData = doc.data()
    }
    setupUserDashboardMenuTable(voteTopicArray, userData)
    // 

}
async function AdminTableHistroy() {
    const yourRefTable = db.collection('Users');
    const snapshot = await yourRefTable.get();
    snapshot.forEach(doc => {
        // console.log(doc.id, '=>', doc.data());
        const data = doc.data()
        setupAdminTable(data)
    });
}

function fileName() {
    var fileUpload = document.getElementById("CSVUpload");
    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
    // console.log(regex);
    if (regex.test(fileUpload.value.toLowerCase())) {

        if (typeof (FileReader) != "undefined") {
            var reader = new FileReader();
            reader.onload = function (e) {
                var rows = e.target.result.split("\n");
                for (var i = 0; i < rows.length; i++) {
                    var cells = rows[i].split(",");
                    if (cells.includes("Email")) {
                        // console.log(cells);
                    }
                    else {
                        if (cells[0] && cells[1] !== "undefined") {

                            const email = cells[0]
                            const shares = cells[1]
                            // console.log()
                            const defaultPassword = "qwe123!"
                            setTimeout(function () {
                                bulkSignUp(email, defaultPassword)
                            }, 3000);
                            setTimeout(function () {
                                bulkAddUser(email, shares)
                                // console.log("ITEM ADD")
                            }, 3000);
                            // console.log(email, shares)
                        }
                    }
                }
            }
            reader.readAsText(fileUpload.files[0]);
        } else {
            alert("This browser does not support HTML5.");
        }
    } else {
        alert("Please upload a valid CSV file.");
    }
}


window.onload = function () {

    let userEmail = ""
    //on auth change
    auth.onAuthStateChanged(user => {
        if (user) {
            userEmail = user && user.email
            // console.log(user, "IM THE USER ")
            let username = document.querySelector(".usrname")
            username.innerHTML = `Welcome ${userEmail}`;
            if (window.location.href.match('UserDashboardMenu.html') != null) {
                getCurrentUserVoteHistory(userEmail)
            }
            else if (window.location.href.match('AdminFullVoteHistory.html') != null) {
                populateAdminVoteTopic()
                AdminTableHistroy();
            }
        }
        else {
            if (window.location.href.match('index.html') == null) {
                if (window.location.href.match('forgetPassword.html') == null) {
                    if (window.location.href.match('signup.html') == null) {
                        // console.log("no permision");
                        window.location.href = "../index.html";
                    }
                }

                if (window.location.href.match('signup.html') == null) {
                    if (window.location.href.match('forgetPassword.html') == null) {
                        // console.log("no permision");
                        window.location.href = "../index.html";
                    }
                }
            }

            // console.log("NO USER FOUND")
        }

    })


    //DATA BASE CONNECTION AND GET BACK DATA

    // if (window.location.href.match('UserDashboardMenu.html') != null) {
    //     db.collection("Users").get().then(snapshot => {
    //         setupUserDashboardMenuTable(snapshot.docs)
    //     })
    // }

    // THIS SECTION IS LOGOUT SECTION
    if (window.location.href.match('UserDashboard.html') != null || window.location.href.match('AdminDashboard.html') != null) {
        var radios = document.querySelectorAll('input[type=radio][name="exampleRadios"]');
        radios.forEach(radio => radio.addEventListener('change', (res) => {
            // console.log(res)
        }))

        document.querySelector("#logout").addEventListener("click", function (e) {
            e.preventDefault();
            logout();
        });

        document.querySelector("#upload").addEventListener("click", function (event) {
            event.preventDefault();
            fileName();
        });
    }

    if (window.location.href.match('AdminDashboard.html') != null) {

        var date = new Date().toISOString().slice(0, 10);
        
            //To restrict past date

            $('#inputDateVoteTopic').attr('min', date);
            $('#inputDateVoteTopic').attr('max', "2100-01-01");

    }

    if (window.location.href.match('AdminVoteTitleEdit.html') != null) {

        populateAdminVoteTopic();
        document.querySelector("#adminVoteTopics").addEventListener("change", function (event) {
            event.preventDefault();


            viewEditForm();
            var selectElement = document.getElementById("adminVoteTopics");

            var selectElementResult = selectElement.options[selectElement.selectedIndex].text;
            // console.log(selectElementResult, "RESULT OF DROP DOWN SELECT")
            //CHECKING THE DROP DOWN TEXT
            if (selectElementResult.includes("-- select an option --")) {
                document.getElementById("EditTopicForm").style.display = "none";
            } else {
                viewEditForm();
            }


            document.querySelector("#updateTopic").addEventListener("click", function (event) {
                event.preventDefault();
                updateTopic();
            })

            // document.querySelector("#deleteTopic").addEventListener("click", function (event) {
            //     event.preventDefault();
            //     deleteTopic();
            // })

            var date = new Date().toISOString().slice(0, 10);
        
            //To restrict past date

            $('#updateDateVoteTopic').attr('min', date);
            $('#updateDateVoteTopic').attr('max', "2100-01-01");
        });


    }


    // THIS SECTION IS LOGIN SECTION 




}


// $(document).ready(function () {
//     $('#AdminTable').DataTable();
//     $('.dataTables_length').addClass('bs-select');
// });