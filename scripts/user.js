let Distinct_Vote_Question_Array =[]

function submitVote() {

    const submitVoteForm = document.querySelector("#submitVoteForm");
    const submitVoteBtn = document.querySelector("#SubmitVote")
    let selectElement = document.getElementById("voteTopics");
    let selectElementResult = selectElement.options[selectElement.selectedIndex].text;

    // console.log(selectElementResult);
    var radioBtn = document.querySelector('input[name="exampleRadios"]:checked').value;
    submitVoteForm.addEventListener("submit", async (e) => {
        if (selectElementResult.includes("-- select an option --")) {
            e.preventDefault();
            // console.log("HELLO")
            submitVoteBtn.disabled = true
            setTimeout(() => {
                submitVoteBtn.disabled = false
            }, 1000);
        }
        else {
            e.preventDefault();
            var user = auth.currentUser;
            submitVoteForm.reset()
            let userEmail = user && user.email

            //CHECK FOR THE selectElementResult BEFORE MOVING ON
            // selectElementResult
            // console.log(Distinct_Vote_Question_Array,"UNIQUE ARRAY")
            Distinct_Vote_Question_Array.map((item)=>{
                const trimmedCheck = selectElementResult.substring(0,51)
                if(item.question.includes(trimmedCheck)){
                    selectElementResult = item.question
                }
            })
            // console.log(existedVoteQuestion,"CHECKING THE EXISTING ARR OF QUESTIONs")

            // console.log(userEmail)
            const topicRef = db.collection('Users').doc(userEmail);
            topicRef.get().then(async (doc) => {
                if (doc.exists) {
                    let documentData = doc.data();

                    // console.log(documentData.VoteTitles,"BEFORE IF STATEMENT")
                    if (documentData && documentData.VoteTitles) {
                        // console.log(documentData.VoteTitles, "CHECKING THE DATA BEFOREHAND")
                        let existedVote = documentData.VoteTitles.filter(eachitem => eachitem.VoteTopic === selectElementResult)
                        let existedVoteIndex = documentData.VoteTitles.findIndex(eachitem => eachitem.VoteTopic === selectElementResult)
                        let shares = documentData && documentData.Shares
                        // console.log(shares, "CHECKINF THE SHARES")
                        // console.log(existedVoteIndex,"CHECK THE INDEX OF ITEM")
                        if (existedVote === undefined || existedVote.length == 0) {
                            const res = await topicRef.set({
                                Email: userEmail,
                                Shares: shares,
                                VoteTitles: [...documentData.VoteTitles, { VoteTopic: selectElementResult, Vote: radioBtn }],
                            }, { merge: true });
                            submitVoteForm.reset()
                            alert(`You Have Voted ${radioBtn} On Vote Topic ${selectElementResult.substring(0,19)}`)
                            window.location.href = "./UserDashboardMenu.html"
                        }
                        else {
                            // var foundIndex = items.findIndex(x => x.id == item.id);
                            const resubmittedVote = { VoteTopic: selectElementResult, Vote: radioBtn }
                            documentData.VoteTitles[existedVoteIndex] = resubmittedVote;
                            // console.log(documentData.VoteTitles, "CHECKING THE DATA AFTER MODIFICATION")
                            const res = await topicRef.set({
                                Email: userEmail,
                                Shares: shares,
                                VoteTitles: [...documentData.VoteTitles],
                            }, { merge: true });
                            submitVoteForm.reset()
                            alert(`You Have Changed Your Vote To ${radioBtn} On Vote Topic ${selectElementResult.substring(0,19)}`)
                            window.location.href = "./UserDashboardMenu.html"
                        }
                    }
                }
                else {
                    // console.log("NOT SUCH DOCUMENT")
                    const res = await topicRef.set({
                        Email: userEmail,
                        Shares: shareWeight,
                        VoteTitles: [{ VoteTopic: selectElementResult, Vote: radioBtn }],
                    });
                }
            })
        }

    })

}

async function populateVoteTopic() {
    let voteTopic = document.querySelector("#voteTopics");
    let Html = `<option value="">-- select an option --</option>`
    const topicRef = db.collection('VoteTopics');
    const snapshot = await topicRef.get();
    snapshot.forEach(snapshot => {
        let eachTopic = snapshot.data() && snapshot.data().question
        const expiryDate = snapshot.data() && snapshot.data().expiryDate
        let voteQuestionArray = []
        voteQuestionArray.push({question:eachTopic})
        Distinct_Vote_Question_Array = [...new Set(voteQuestionArray)]
        const dateNow = new Date().getTime()
        if (dateNow > expiryDate) {
            // alert(`Vote Topic ${eachTopic} Has Been Expired`)
        }
        else {
            if (eachTopic && eachTopic.length > 81) {
                let trimmedVoteTopic = eachTopic.substring(0, 81)
                trimmedVoteTopic = `${trimmedVoteTopic} ...`
                const option = `
            <option value=${eachTopic}>${trimmedVoteTopic}</option>
            `
                Html += option
            }
            else {
                const option = `
            <option value=${eachTopic}>${eachTopic}</option>
            `
                Html += option
            }
            // const option = `
            // <option value=${eachTopic}>${eachTopic}</option>
            // `
            // // selectElement.options[selectElement.selectedIndex].text;
            // Html += option

        }
    });

    voteTopic.innerHTML = Html
    // var user = auth.currentUser;
    // if(user){
    //     username.innerHTML = `Welcome ${userEmail}`;
    //     let username = document.querySelector(".usrname")
    // }
    // else console.log("USER NOT FOUND")


}

const setupUserDashboardMenuTable = (VoteTopicData, userData) => {
    let resultTable = document.querySelector("#results");
    let html = ""
    let voteTitle = ""
    let vote = ""
    let isVoted = false
    let voteStatus = ""
    shares = userData && userData.Shares

    if (VoteTopicData) {
        VoteTopicData.map((eachVoteItem) => {
            // console.log(eachVoteItem, "EACH VOTE")
            let existedVote = userData.VoteTitles.filter(eachitem => eachitem.VoteTopic === eachVoteItem.question)
            // console.log(existedVote[0].VoteTopic, "existed VOTE")

            if (existedVote && existedVote[0] && existedVote[0].VoteTopic == eachVoteItem.question) {
                isVoted = true
                vote = existedVote[0].Vote
            } else {
                isVoted = false

            }
            // existedVote && existedVote.map((existedItem)=>{

            //     if(eachVoteItem.question == existedItem.VoteTopic){
            //         isVoted = true
            //         vote = existedItem.Vote
            //     }else {
            //         isVoted = false

            //     }

            // })
            // if (existedVote.length > 0) {
            //     existedVote.map((eachVote) => {
            //         // console.log(eachItem.question ,"question from voteTopics Table")
            //         // console.log(eachVote.VoteTopic ,"vote topic from Users Table")
            //         if(eachVoteItem.question === eachVote.VoteTopic){

            //             vote = eachVote.Vote
            //             isVoted = true
            //         }
            //         else{
            //             isVoted = false

            //         }
            //     })
            // }
            const dateNow = new Date().getTime()
            if (eachVoteItem.expiryDate > dateNow) {
                voteStatus = "Open"
            }
            else {
                voteStatus = "Closed"
            }
            // console.log(existedVote, "check the filter")
            const tr = `
                        <tr>
                            <td>${eachVoteItem.question}</td>
                            <td>${isVoted ? vote : "Note Voted"}</td>
                            <td>${voteStatus}</td>
                            <td>${shares}</td>
                        </tr>
                        `;
            html += tr;
        })
        resultTable.insertAdjacentHTML('beforeend', html);
        $(document).ready(function () {
            $('#table').DataTable();
            $('.dataTables_length').addClass('bs-select');
        });
    }
    resultTable.innerHTML = html

}
function displayTopic() {
    var selectElement = document.getElementById("voteTopics");
    // var selectElementResult = selectElement.options[selectElement.selectedIndex].text;
    let selectElementResult = $("#voteTopics :selected").text()

    Distinct_Vote_Question_Array.map((item)=>{
        const trimmedCheck = selectElementResult.substring(0,51)
        if(item.question.includes(trimmedCheck)){
            selectElementResult = item.question
        }
    })
    
    // console.log(abc)
    let chosenElement = document.querySelector("#chosenTopic")
    if (selectElementResult === "-- select an option --") {
        chosenElement.innerHTML = ""
    }
    else {

        chosenElement.innerHTML = selectElementResult
    }
    // console.log(selectElementResult);
}
window.onload = function () {
    populateVoteTopic()


}


$(document).ready(function () {
    $('#voteTopics').on('change', function () {
        // displayTopic()
        displayTopic()
    });
    //  document.querySelector("#voteTopics").addEventListener('change', displayTopic());
})